# Questionnaire

# Local setup
- Pull code from this repository
- Create empty postgres database
- Point to database in /server/database/config/config.json
- Run 'npm install'
- Run 'nodemon app' from terminal

# Docker setup
Image is uploaded to 'https://hub.docker.com/r/dockingmaster/questionnaire/' and can be downloaded by running command 'docker pull dockingmaster/questionnaire'

- Pull code from this repository or at least download 'docker-compose.yml' file
- Navigate to directory which contains 'docker-compose.yml' file
- Run 'docker-compose up'

# Postman setup
- Postman enviroment and collection can be found in /postman directory
- Postman collection contains examples for all available api calls (only ids might need to be changed)
- Enviroment is configured for docker container to point on default port
- For development mode 'server' variable should be updated with port 3000
