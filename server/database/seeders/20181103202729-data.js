const bcrypt = require('bcrypt');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Users', [
      {
        id: 1,
        firstName: 'admin',
        lastName: 'admin',
        email: 'admin@admin.mop',
        password: bcrypt.hashSync('Password', bcrypt.genSaltSync(8)),
        isAdmin: true
      },
      {
        id: 2,
        firstName: 'user',
        lastName: 'user',
        email: 'user@user.mop',
        password: bcrypt.hashSync('Password', bcrypt.genSaltSync(8)),
        isAdmin: false
      }
    ], {});
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Users', null, {});
  }
};
