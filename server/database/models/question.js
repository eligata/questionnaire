'use strict';
module.exports = (sequelize, DataTypes) => {
  const Question = sequelize.define('Question', {
    body: DataTypes.STRING,
    questionType: DataTypes.ENUM('Text', 'YesNo', 'SingleChoice', 'MultipleChoice')
  }, {
      timestamps: false
    });
  Question.associate = function (models) {
    Question.belongsTo(models.Questionnaire, { onDelete: "CASCADE" });
    Question.hasMany(models.PossibleAnswer);
    Question.hasMany(models.Answer);
  };
  return Question;
};