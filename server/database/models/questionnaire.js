'use strict';
module.exports = (sequelize, DataTypes) => {
  const Questionnaire = sequelize.define('Questionnaire', {
    title: DataTypes.STRING
  }, {
      timestamps: false
    });
  Questionnaire.associate = function (models) {
    Questionnaire.belongsTo(models.User, { onDelete: "CASCADE" });
    Questionnaire.hasMany(models.Question);
    Questionnaire.hasMany(models.CompletedQuestionnaire);
  };
  return Questionnaire;
};