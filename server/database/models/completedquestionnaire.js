'use strict';
module.exports = (sequelize, DataTypes) => {
  const CompletedQuestionnaire = sequelize.define('CompletedQuestionnaire', {
    completedAt: DataTypes.DATE
  },
    {
      timestamps: false
    });
  CompletedQuestionnaire.associate = function (models) {
    CompletedQuestionnaire.belongsTo(models.Questionnaire, { onDelete: "CASCADE" });
    CompletedQuestionnaire.belongsTo(models.User, { onDelete: "CASCADE" });
  };
  return CompletedQuestionnaire;
};