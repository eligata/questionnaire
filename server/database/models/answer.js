'use strict';
module.exports = (sequelize, DataTypes) => {
  const Answer = sequelize.define('Answer', {
    body: DataTypes.STRING
  }, {
      timestamps: false
    });
  Answer.associate = function (models) {
    Answer.belongsTo(models.Question, { onDelete: "CASCADE" });
    Answer.belongsTo(models.User, { onDelete: "CASCADE" });
    Answer.belongsTo(models.PossibleAnswer, { onDelete: "CASCADE" });
  };
  return Answer;
};