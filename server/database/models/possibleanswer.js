'use strict';
module.exports = (sequelize, DataTypes) => {
  const PossibleAnswer = sequelize.define('PossibleAnswer', {
    body: DataTypes.STRING
  }, {
      timestamps: false
    });
  PossibleAnswer.associate = function (models) {
    PossibleAnswer.belongsTo(models.Question, { onDelete: "CASCADE" });
  };
  return PossibleAnswer;
};