'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    isAdmin: DataTypes.BOOLEAN
  }, {
      timestamps: false
    });
  User.associate = function (models) {
    User.hasMany(models.Questionnaire);
    User.hasMany(models.Answer);
    User.hasMany(models.CompletedQuestionnaire);
  };
  return User;
};