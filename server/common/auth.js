const jwt = require('jsonwebtoken');
const config = require('../../config')

module.exports = function authenticate(req, res, next) {
    var authToken = req.headers['x-authorize'];

    // Token is required for authentication
    if (!authToken)
        res.status(401).json({ message: config.Messages.LoginRequired });

    jwt.verify(authToken, config.JwtSecret, (error, data) => {
        if (error)
            res.status(401).json({ message: config.Messages.InvalidAuthtoken });
        else {
            req.user = data;
            next();
        }
    });
}