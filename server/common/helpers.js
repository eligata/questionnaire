exports.processResult = (result, res) => {
    var affectedRows = 0;

    if (result.constructor === Array && result.length > 0)
        affectedRows = result[0];

    else if (result.constructor === Number)
        affectedRows = result;

    res.status(200).json({ message: affectedRows + " row(s) affected." });
};