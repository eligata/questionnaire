const questionRepository = require('../repositories/question.repository');
const messages = require('../../config').Messages;
const helpers = require('../common/helpers');

exports.getQuestionById = async (req, res, next) => {
    try {
        var question = await questionRepository.getQuestionById(req.params.id);
        if (!question)
            return res.status(200).json({ message: messages.NonExistingQuestion });

        res.status(200).json({ question });
    }
    catch (error) {
        return res.status(500).json({
            message: error.message
        });
    }
};

exports.createQuestions = async (req, res, next) => {
    var payload = req.body;
    var questions = [];

    try {
        for (var i = 0; i < payload.questions.length; i++) {
            var question = payload.questions[i];

            questions.push({
                body: question.body,
                QuestionnaireId: payload.questionnaireId,
                questionType: question.questionType
            });
        }

        var result = await questionRepository.createQuestions(questions);
        res.status(200).json({ result });
    }
    catch (error) {
        return res.status(500).json({
            message: error.message
        });
    }
};

exports.updateQuestion = async (req, res, next) => {
    var payload = req.body;

    try {
        var question = await questionRepository.getQuestionById(req.params.id);
        if (!question)
            return res.status(400).json({ message: messages.NonExistingQuestion });

        var result = await questionRepository.updateQuestion(req.params.id, {
            body: payload.body,
            QuestionnaireId: payload.questionnaireId,
            questionType: payload.questionId
        });
        helpers.processResult(result, res);;
    }
    catch (error) {
        return res.status(500).json({
            message: error.message
        });
    }
};

exports.deleteQuestion = async (req, res, next) => {
    try {
        var question = await questionRepository.getQuestionById(req.params.id);
        if (!question)
            return res.status(400).json({ message: messages.NonExistingQuestion });

        var result = await questionRepository.deleteQuestion(req.params.id);
        helpers.processResult(result, res);
    }
    catch (error) {
        return res.status(500).json({
            message: error.message
        });
    }
};