const questionRepository = require('../repositories/question.repository');
const possibleAnswerRepository = require('../repositories/possible-answer.repository');
const messages = require('../../config').Messages;
const helpers = require('../common/helpers');

exports.getPossibleAnswerById = async (req, res, next) => {
    try {
        var result = await possibleAnswerRepository.getPossibleAnswerById(req.params.id);
        res.status(200).json({ result });
    }
    catch (error) {
        return res.status(500).json({
            message: error.message
        });
    }
};

exports.createPossibleAnswers = async (req, res, next) => {
    var payload = req.body;

    var possibleAnswers = [];

    try {
        for (var i = 0; i < payload.possibleAnswers.length; i++) {
            var possibleAnswer = payload.possibleAnswers[i];

            var question = await questionRepository.getQuestionById(possibleAnswer.questionId);
            if (!question)
                return res.status(400).json({ message: messages.NonExistingQuestion });

            if (question.questionType === "Text" || question.questionType === "YesNo")
                return res.status(400).json({ message: messages.PossibleAnswersNotAllowed });

            possibleAnswers.push({
                body: possibleAnswer.body,
                QuestionId: possibleAnswer.questionId
            });
        }

        var result = await possibleAnswerRepository.createPossibleAnswers(possibleAnswers);
        res.status(200).json({ result });
    }
    catch (error) {
        return res.status(500).json({
            message: error.message
        });
    }
};

exports.updatePossibleAnswer = async (req, res, next) => {
    var payload = req.body;

    try {
        var question = await questionRepository.getQuestionById(payload.questionId);
        if (!question)
            return res.status(400).json({ message: messages.NonExistingQuestion });

        if (question.questionType === "Text" || question.questionType === "YesNo")
            return res.status(400).json({ message: messages.PossibleAnswersNotAllowed });

        var result = await possibleAnswerRepository.updatePossibleAnswer(req.params.id, {
            body: payload.body,
            QuestionnId: payload.questionId
        });
        helpers.processResult(result, res); 
    }
    catch (error) {
        return res.status(500).json({
            message: error.message
        });
    }
};

exports.deletePossibleAnswer = async (req, res, next) => {
    try {
        var result = await possibleAnswerRepository.deletePossibleAnswer(req.params.id);
        helpers.processResult(result, res);
    }
    catch (error) {
        return res.status(500).json({
            message: error.message
        });
    }
};