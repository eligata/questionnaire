const Joi = require('joi');

function validate() {
    return {
        body: {
            questionnaireId: Joi.number().required(),
            answers: Joi.array().items().required()
        }
    }
};
module.exports = {
    validate: validate()
}