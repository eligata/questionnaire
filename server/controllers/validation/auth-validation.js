const Joi = require('joi');

function loginModel() {
    return {
        body: {
            email: Joi.string().email().required(),
            password: Joi.string().regex(/[a-zA-Z0-9]{3,30}/).required()
        }
    }
};

function registerModel() {
    return {
        body: {
            email: Joi.string().email().required(),
            password: Joi.string().regex(/[a-zA-Z0-9]{8,30}/).required(),
            firstName: Joi.string().required(),
            lastName: Joi.string().required(),
        }
    }
};

function resetPasswordModel() {
    return {
        body: {
            oldPassword: Joi.string().regex(/[a-zA-Z0-9]{8,30}/).required(),
            newPassword: Joi.string().regex(/[a-zA-Z0-9]{8,30}/).required()
        }
    }
};

module.exports = {
    loginModel: loginModel(),
    registerModel: registerModel(),
    resetPasswordModel: resetPasswordModel()
}