const Joi = require('joi');

function createModel() {
    return {
        body: {
            questionnaireId: Joi.number().required(),
            questions: Joi.array().items(Joi.object({
                body: Joi.string().required(),
                questionType: Joi.string().valid('Text', 'YesNo', 'SingleChoice', 'MultipleChoice').required()
            })).required()
        }
    }
};

function updateModel() {
    return {
        body: {
            questionnaireId: Joi.number().required(),
            body: Joi.string().required(),
            questionType: Joi.string().valid('Text', 'YesNo', 'SingleChoice', 'MultipleChoice').required()
        }
    }
}

module.exports = {
    createModel: createModel(),
    updateModel: updateModel()
}