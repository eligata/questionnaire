const Joi = require('joi');

function createModel() {
    return {
        body: {
            possibleAnswers: Joi.array().items(Joi.object({
                body: Joi.string().required(),
                questionId: Joi.number().required()
            })).required()
        }
    }
};

function updateModel() {
    return {
        body: {
            body: Joi.string().required(),
            questionId: Joi.number().required()
        }
    }
}

module.exports = {
    createModel: createModel(),
    updateModel: updateModel()
}