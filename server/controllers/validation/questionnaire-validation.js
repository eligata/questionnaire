const Joi = require('joi');

function model() {
    return {
        body: {
            title: Joi.string().required()
        }
    }
};

module.exports = {
    model: model()
}