const questionnaireRepository = require('../repositories/questionnaire.repository');
const messages = require('../../config').Messages;
const helpers = require('../common/helpers');

exports.getAllQuestionnaires = async (req, res, next) => {
    try {
        var questionnaires = await questionnaireRepository.getAllQuestionnaires();
        if (!questionnaires || questionnaires.length === 0)
            return res.status(200).json({ message: messages.NoQuestionnaires });

        res.status(200).json({ questionnaires });
    }
    catch (error) {
        return res.status(500).json({
            message: error.message
        });
    }
};


exports.getMyQuestionnaires = async (req, res, next) => {
    try {
        var questionnaires = await questionnaireRepository.getMyQuestionnaires(req.user.id);
        if (!questionnaires || questionnaires.length === 0)
            return res.status(200).json({ message: messages.NoQuestionnaires });

        res.status(200).json({ questionnaires });
    }
    catch (error) {
        return res.status(500).json({
            message: error.message
        });
    }
};

exports.getAvailableQuestionnaires = async (req, res, next) => {
    try {
        var questionnaires = await questionnaireRepository.getAvailableQuestionnaires(req.user.id);
        if (!questionnaires || questionnaires.length === 0)
            return res.status(200).json({ message: messages.NoAvailableQuestionnaires });

        res.status(200).json({ questionnaires });
    }
    catch (error) {
        return res.status(500).json({
            message: error.message
        });
    }
};

exports.getQuestionnaireById = async (req, res, next) => {
    try {
        var questionnaire = await questionnaireRepository.getQuestionnaireById(req.params.id);
        if (!questionnaire)
            return res.status(200).json({ message: messages.NonExistingQuestionnaire });

        res.status(200).json({ questionnaire });
    }
    catch (error) {
        return res.status(500).json({
            message: error.message
        });
    }
};

exports.createQuestionnaire = async (req, res, next) => {
    var payload = req.body;

    try {
        var result = await questionnaireRepository.createQuestionnaire({
            title: payload.title,
            UserId: req.user.id
        });
        res.status(200).json({ result });
    }
    catch (error) {
        return res.status(500).json({
            message: error.message
        });
    }
};

exports.updateQuestionnaire = async (req, res, next) => {
    var payload = req.body;

    try {
        var questionnaire = await questionnaireRepository.getQuestionnaireById(req.params.id);
        if (!questionnaire)
            return res.status(400).json({ message: messages.NonExistingQuestionnaire });

        if (questionnaire.UserId !== req.user.id)
            return res.status(400).json({ message: messages.CantTouchQuestionnaire });

        var result = await questionnaireRepository.updateQuestionnaire(req.params.id, {
            title: payload.title,
            UserId: req.user.id
        });
        helpers.processResult(result, res);
    }
    catch (error) {
        return res.status(500).json({
            message: error.message
        });
    }
};

exports.deleteQuestionnaire = async (req, res, next) => {
    try {
        var questionnaire = await questionnaireRepository.getQuestionnaireById(req.params.id);
        if (!questionnaire)
            return res.status(400).json({ message: messages.NonExistingQuestionnaire });

        if (questionnaire.UserId !== req.user.id)
            return res.status(400).json({ message: messages.CantTouchQuestionnaire });

        var result = await questionnaireRepository.deleteQuestionnaire(req.params.id);
        helpers.processResult(result, res);
    }
    catch (error) {
        return res.status(500).json({
            message: error.message
        });
    }
};