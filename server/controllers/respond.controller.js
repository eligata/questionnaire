const respondRepository = require('../repositories/respond.repository');
const questionnaireRepository = require('../repositories/questionnaire.repository');
const messages = require('../../config').Messages;

exports.createQuestionnaireResponse = async (req, res, next) => {
    var payload = req.body;
    var answers = [];
    var errors = [];

    var questionnaire = await questionnaireRepository.getQuestionnaireById(payload.questionnaireId);
    if (!questionnaire)
        return res.status(400).json({ message: messages.NonExistingQuestionnaire });

    var isCompleted = await questionnaireRepository.hasUserCompletedQuestionnaire(req.user.id, payload.questionnaireId);
    if (isCompleted)
        return res.status(400).json({ message: 'You completed this questionnaire on "' + isCompleted.completedAt + '"' });

    for (var i = 0; i < questionnaire.Questions.length; i++) {
        var question = questionnaire.Questions[i];
        var submittedQuestionRespond = payload.answers.find(e => { return e.questionId === question.id; });
        
        if (!submittedQuestionRespond){
            errors.push("Question " + question.id + " require answer.");
            continue; 
        }
        // Text and YesNo questions should be answered with body
        if (question.questionType === 'Text' || question.questionType === 'YesNo') {
            if (!submittedQuestionRespond.body) {
                errors.push("Question " + question.id + " require " + question.questionType + " answer.");
                continue;
            }
            else {
                if (question.questionType === 'YesNo' &&
                    submittedQuestionRespond.body &&
                    submittedQuestionRespond.body.toLowerCase() !== "yes" &&
                    submittedQuestionRespond.body.toLowerCase() !== "no") {
                    errors.push("Question " + question.id + " require " + question.questionType + " answer.");
                    continue;
                }

                if (submittedQuestionRespond.choosenAnswers && submittedQuestionRespond.choosenAnswers.length > 0) {
                    errors.push("Question " + question.id + " accepts only " + question.questionType + " answer.");
                    continue;
                }
            }

            // Create answer
            answers.push({
                body: submittedQuestionRespond.body,
                QuestionId: question.id,
                PossibleAnswerId: null,
                UserId: req.user.id
            });
        }

        // SingleChoice and MultipleChoice questions should be answered onyl with choosen possible answer(s)
        if (question.questionType === 'SingleChoice' || question.questionType === 'MultipleChoice') {
            if (submittedQuestionRespond.body) {
                errors.push("Question " + question.id + " require " + question.questionType + " answer.");
                continue;
            }

            if (!submittedQuestionRespond.choosenAnswers) {
                errors.push("Question " + question.id + " requires choosen answer(s).");
                continue;
            }

            if (submittedQuestionRespond.choosenAnswers.length === 0) {
                errors.push("Question " + question.id + " requires choosen answer(s).");
                continue;
            }


            if (question.questionType === 'SingleChoice' && (!submittedQuestionRespond.choosenAnswers || submittedQuestionRespond.choosenAnswers.length > 1)) {
                errors.push("Question " + question.id + " accepts only one choosen answer.");
                continue;
            }

            for (var j = 0; j < submittedQuestionRespond.choosenAnswers.length; j++) {
                var choosenAnswer = submittedQuestionRespond.choosenAnswers[j];
                var matchingPossibleAnswer = question.PossibleAnswers.find(e => { return e.id === choosenAnswer; });

                if (!matchingPossibleAnswer) {
                    errors.push("Question " + question.id + " doesn't have answer that you offered!");
                    continue;
                }

                // Create answer
                answers.push({
                    body: '',
                    QuestionId: question.id,
                    PossibleAnswerId: matchingPossibleAnswer.id,
                    UserId: req.user.id
                });
            }
        }
    }

    // If validation error(s) exists then it should be returned to user
    if (errors.length > 0) {
        return res.status(400).json({
            errors,
            hint: {
                Text: "Textual answer is accepted!",
                YesNo: "Yes or No are accepted",
                SingleChoice: "Only one possible answer is accepted",
                MultipleChoice: "At least one possible answer is accepted",
            }
        });
    }

    try {
        var result = await respondRepository.createQuestionnaireResponse(answers);

        if (result) {
            var completed = await questionnaireRepository.createCompletedQuestionnaire({
                UserId: req.user.id,
                QuestionnaireId: payload.questionnaireId,
                completedAt: new Date()
            });
        }

        res.status(200).json({ message: messages.CompletedQuestionnaire });
    }
    catch (error) {
        return res.status(500).json({
            message: error.message
        });
    }
};