const authRepository = require('../repositories/auth.repository');
const bcrypt = require('bcrypt');
const messages = require('../../config').Messages;


exports.login = async (req, res, next) => {
    var payload = req.body;

    try {
        var user = await authRepository.getUserByEmail(payload.email);
        if (!user)
            return res.status(400).json({ message: messages.InvalidUsernameOrPassword });

        var passwordIsValid = bcrypt.compareSync(payload.password, user.password);
        if (!passwordIsValid)
            return res.status(400).json({ message: messages.InvalidUsernameOrPassword });

        var authToken = await authRepository.getToken(user);
        res.status(200).json({ authToken });
    }
    catch (error) {
        return res.status(500).json({
            message: error.message
        });
    }
};

exports.register = async (req, res, next) => {
    var payload = req.body;

    try {
        var user = await authRepository.getUserByEmail(payload.email);
        if (user)
            return res.status(400).json({ message: messages.EmailInUse });

        var createdUser = await authRepository.createUser({
            email: payload.email,
            password: bcrypt.hashSync(payload.password, bcrypt.genSaltSync(8)),
            firstName: payload.firstName,
            lastName: payload.lastName,
            isAdmin: false
        });

        var authToken = await authRepository.getToken(createdUser);

        res.status(200).json({ authToken });
    }
    catch (error) {
        return res.status(500).json({
            message: error.message
        });
    }
};

exports.resetPassword = async (req, res, next) => {
    var payload = req.body;

    try {
        var user = await authRepository.getUserByEmail(req.user.email);
        if (!user)
            return res.status(400).json({ message: messages.InvalidUsernameOrPassword });

        var passwordIsValid = bcrypt.compareSync(payload.oldPassword, user.password);
        if (!passwordIsValid)
            return res.status(400).json({ message: messages.InvalidUsernameOrPassword });

        await authRepository.resetPassword(user.id, { password: bcrypt.hashSync(payload.newPassword, bcrypt.genSaltSync(8)) });
        
        return res.status(400).json({ message: messages.PasswordChanged });
    }
    catch (error) {
        return res.status(500).json({
            message: error.message
        });
    }
};