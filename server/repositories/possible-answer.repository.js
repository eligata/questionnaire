const possibleAnswer = require('../database/models').PossibleAnswer;

exports.getPossibleAnswerById = async (id) => {
    return possibleAnswer
        .findOne({ where: { id: id } })
        .then(result => { return result })
        .catch(error => { throw error });
};

exports.createPossibleAnswers = async (model) => {
    return possibleAnswer.bulkCreate(model, { individualHooks: true })
        .then(result => { return result })
        .catch(error => { throw error });
};

exports.updatePossibleAnswer = async (id, model) => {
    return possibleAnswer.update(model, { where: { id: id } })
        .then(result => { return result })
        .catch(error => { throw error });
};

exports.deletePossibleAnswer = async (id) => {
    return possibleAnswer.destroy({ where: { id: id } })
        .then(result => { return result })
        .catch(error => { throw error });
};