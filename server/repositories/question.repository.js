const question = require('../database/models').Question;

exports.getQuestionById = async (id) => {
    return question
        .findOne({ where: { id: id } })
        .then(result => { return result })
        .catch(error => { throw error });
};

exports.createQuestions = async (model) => {
    return question.bulkCreate(model, { individualHooks: true })
        .then(result => { return result })
        .catch(error => { throw error });
};

exports.updateQuestion = async (id, model) => {
    return question.update(model, { where: { id: id } })
        .then(result => { return result })
        .catch(error => { throw error });
};

exports.deleteQuestion = async (id) => {
    return question.destroy({ where: { id: id } })
        .then(result => { return result })
        .catch(error => { throw error });
};