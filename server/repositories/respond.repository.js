const answer = require('../database/models').Answer;

exports.createQuestionnaireResponse = async (answers) => {
    return answer.bulkCreate(answers, { individualHooks: true })
        .then(result => { return result })
        .catch(error => { throw error });
};