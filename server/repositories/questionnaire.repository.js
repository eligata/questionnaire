const db = require('../database/models');
const questionnaire = require('../database/models').Questionnaire;
const question = require('../database/models').Question;
const possibleAnswer = require('../database/models').PossibleAnswer;
const completedQuestionnaire = require('../database/models').CompletedQuestionnaire;

exports.getAllQuestionnaires = async () => {
    return questionnaire.findAll({ include: [{ model: question, include: [{ model: possibleAnswer }] }] })
        .then(result => { return result; })
        .catch(error => { throw error });
};

exports.getMyQuestionnaires = async (userId) => {
    return questionnaire.findAll({ include: [{ model: question, include: [{ model: possibleAnswer }] }] }, { where: { userId: userId } })
        .then(result => { return result; })
        .catch(error => { throw error });
};

exports.getAvailableQuestionnaires = async (userId) => {
    return questionnaire
        .findAll(
            {
                where: {
                    id: { [db.sequelize.Op.notIn]:db.sequelize.literal(' (SELECT "QuestionnaireId" FROM "CompletedQuestionnaires" WHERE "UserId" = ' + userId + ')')}
                },
                include: [{
                    model: completedQuestionnaire,
                    attributes: []
                },
                {
                    model: question,
                    include: [{ model: possibleAnswer }]
                }]
            }
        )
        .then(result => { return result; })
        .catch(error => { throw error });
};

exports.getQuestionnaireById = async (id) => {
    return questionnaire.findOne({ include: [{ model: question, include: [{ model: possibleAnswer }] }], where: { id: id } })
        .then(result => { return result; })
        .catch(error => { throw error });
};

exports.createQuestionnaire = async (model) => {
    return questionnaire.create(model)
        .then(result => { return result })
        .catch(error => { throw error });
};

exports.updateQuestionnaire = async (id, model) => {
    return questionnaire.update(model, { where: { id: id } })
        .then(result => { return result })
        .catch(error => { throw error });
};

exports.deleteQuestionnaire = async (id) => {
    return questionnaire.destroy({ where: { id: id } })
        .then(result => { return result })
        .catch(error => { throw error });
};

exports.hasUserCompletedQuestionnaire = async (userId, questionnaireId) => {
    return completedQuestionnaire.findOne({ where: { UserId: userId, QuestionnaireId: questionnaireId }, attributes: ['completedAt'] })
        .then(result => { return result })
        .catch(error => { throw error });
};

exports.createCompletedQuestionnaire = async (model) => {
    return completedQuestionnaire.create(model)
        .then(result => { return result })
        .catch(error => { throw error });
};