const jwt = require('jsonwebtoken');
const config = require('../../config');
const bcrypt = require('bcrypt');
const user = require('../database/models').User;

exports.getUserByEmail = async function (email) {
    return user
        .findOne({ where: { email: email } })
        .then(result => { return result })
        .catch(error => { throw error });
}

exports.createUser = async function (model) {
    console.log(model);
    
    return user
        .create(model)
        .then(result => { return result })
        .catch(error => { throw error });
}

exports.resetPassword = async function (id, model) {
    return user
        .update(model, { where: { id: id } })
        .then(result => { return result; })
        .catch(error => { throw error });
}

exports.getToken = async function (user) {
    var result = await jwt.sign(
        {
            id: user.id,
            isAdmin: user.isAdmin,
            email: user.email
        },
        config.JwtSecret,
        {
            expiresIn: config.JwtExpiresIn
        });

    return result;
}