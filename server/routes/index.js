const express = require("express");
const router = express.Router();

router.use('/auth', require('./auth'));
router.use('/questionnaire', require('./questionnaire'));
router.use('/question', require('./question'));
router.use('/possibleanswer', require('./possible-answer'));
router.use('/respond', require('./respond'));

module.exports = router;
