const express = require("express");
const router = express.Router();
const authorize = require('../common/auth');
const authController = require('../controllers/auth.controller');
const validate = require('express-validation')
const authValidation = require('../controllers/validation/auth-validation');

module.exports = router.post('/login', validate(authValidation.loginModel), authController.login);
module.exports = router.post('/register', validate(authValidation.registerModel), authController.register);
module.exports = router.post('/resetPassword', validate(authValidation.resetPasswordModel), authorize, authController.resetPassword);