const express = require("express");
const router = express.Router();
const authorize = require('../common/auth');
const adminOnly = require('../common/require-admin');
const questionController = require('../controllers/question.controller');
const validate = require('express-validation')
const questionValidation = require('../controllers/validation/question-validation');

module.exports = router.get('/:id', authorize, adminOnly, questionController.getQuestionById);
module.exports = router.post('/', authorize, adminOnly, validate(questionValidation.createModel), questionController.createQuestions);
module.exports = router.put('/:id', authorize, adminOnly, validate(questionValidation.updateModel), questionController.updateQuestion);
module.exports = router.delete('/:id', authorize, adminOnly, questionController.deleteQuestion);