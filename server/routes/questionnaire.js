const express = require("express");
const router = express.Router();
const authorize = require('../common/auth');
const adminOnly = require('../common/require-admin');
const questionnaireController = require('../controllers/questionnaire.controller');
const validate = require('express-validation')
const questionnaireValidation = require('../controllers/validation/questionnaire-validation');

module.exports = router.get('/all', authorize, adminOnly, questionnaireController.getAllQuestionnaires);
module.exports = router.get('/my', authorize, adminOnly, questionnaireController.getMyQuestionnaires);
module.exports = router.post('/', authorize, adminOnly, validate(questionnaireValidation.model), questionnaireController.createQuestionnaire);
module.exports = router.put('/:id', authorize, adminOnly, validate(questionnaireValidation.model), questionnaireController.updateQuestionnaire);
module.exports = router.delete('/:id', authorize, adminOnly, questionnaireController.deleteQuestionnaire);

module.exports = router.get('/:id', authorize, questionnaireController.getQuestionnaireById);
module.exports = router.get('/', authorize, questionnaireController.getAvailableQuestionnaires);