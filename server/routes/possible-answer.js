const express = require("express");
const router = express.Router();
const authorize = require('../common/auth');
const adminOnly = require('../common/require-admin');
const possibleAnswerController = require('../controllers/possible-answer.controller');
const validate = require('express-validation')
const possibleAnswerValidation = require('../controllers/validation/possible-answer-validation');

module.exports = router.get('/:id', authorize, adminOnly, possibleAnswerController.getPossibleAnswerById);
module.exports = router.post('/', authorize, adminOnly, validate(possibleAnswerValidation.createModel), possibleAnswerController.createPossibleAnswers);
module.exports = router.put('/:id', authorize, adminOnly, validate(possibleAnswerValidation.updateModel), possibleAnswerController.updatePossibleAnswer);
module.exports = router.delete('/:id', authorize, adminOnly, possibleAnswerController.deletePossibleAnswer);