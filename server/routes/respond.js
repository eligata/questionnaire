const express = require("express");
const router = express.Router();
const authorize = require('../common/auth');
const respondController = require('../controllers/respond.controller');
const validate = require('express-validation')
const respondValidation = require('../controllers/validation/respond-validation');

module.exports = router.post('/', authorize, validate(respondValidation.validate), respondController.createQuestionnaireResponse);