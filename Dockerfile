FROM node:10
WORKDIR /app

COPY package-lock.json .
COPY package.json .
RUN npm install
COPY . .

CMD node app.js