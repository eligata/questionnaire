module.exports = {
    JwtSecret: '8f52d1d43b14476c899d3e10507ff0956232ebd061a340f7b4c77bd21acb217efbc0720dda0f421da288f0c4b609ab49ad5677a2fc67446988764c2eee0be4ef',
    JwtExpiresIn: 86400,
    Messages: {
        LoginRequired: "You must login to access requesting resources.",
        InvalidUsernameOrPassword: "Invalid username or password. Please try again.",
        InvalidAuthtoken: "Invalid auth token.",
        EmailInUse: "Email already in use.",
        PasswordChanged: "Password cganged successfully.",
        PossibleAnswersNotAllowed: "Question doesn't allow possible answers.",
        NonExistingQuestion: "Question doesn't exist.",
        NonExistingQuestionnaire: "Questionnaire doesn't exist.",
        NoQuestionnaires: "There is no questionnaires yet.",
        NoAvailableQuestionnaires: "There is no available questionnaires at the moment. Please try again later.",
        CantTouchQuestionnaire: "You can't update nor delete questionnaire which you didn't created.",
        CompletedQuestionnaire: "Questionnaire completed successfully.",
        CreatedSuccessfully: "Created successfully.",
        UpdatedSuccessfully: "Updated successfully.",
        DeletedSuccessfully: "Deleted successfully."
    }
};