const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const routes = require('./server/routes');
const models = require('./server/database/models');

var Umzug = require('umzug');
var umzug = new Umzug({
  sequelize: models.sequelize,
  migrations: {
    path: "./server/database/seeders",
    params: [models.sequelize.getQueryInterface(), models.Sequelize]
  }
});

const PORT = process.env.PORT || 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Use configured routes
app.use('/api', routes);

// Connect to database
// Run migrations if needed
// Start app on configured port
models.sequelize.sync().then(() => {
  umzug.up().then(function (migrations) {
    app.listen(PORT, () => {
      console.log('App is running . . .');
    });
  })
});

// Error handler, required for validation as of 0.3.0
app.use(function (err, req, res, next) {
  res.status(400).json(err);
});